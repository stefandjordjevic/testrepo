/*=============================================================


Version:   0.00.0000
Server:    QJ9JY6Y2\SQLEXPRESS

DATABASE:	AdventureWorks2014
  Views:  vProductAndDescription


=============================================================*/
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_WARNINGS ON
SET NOCOUNT ON
SET XACT_ABORT ON
GO

-- Create View [Production].[vProductAndDescription]
Print 'Create View [Production].[vProductAndDescription]'
GO
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO


CREATE VIEW [Production].[vProductAndDescription] 
AS 
-- View (indexed or standard) to display products and product descriptions by language.
SELECT 
    *
FROM [Production].[Product] 
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

