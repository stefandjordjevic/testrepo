/*=============================================================


Version:   1.01.0002
Server:    Q6B4V6Y2\SQLEXPRESS

DATABASE:	AdventureWorks2014
  Data types:  NameStyle


=============================================================*/
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_WARNINGS ON
SET NOCOUNT ON
SET XACT_ABORT ON
GO

-- Create Type [dbo].[NameStyle]
Print 'Create Type [dbo].[NameStyle]'
GO
CREATE TYPE [dbo].[NameStyle]
	FROM [bit]
	NOT NULL
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_WARNINGS ON
SET NOCOUNT ON
SET XACT_ABORT ON
GO
